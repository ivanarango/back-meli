import cors from "cors";
import express from "express";
import morgan from "morgan";
import { SERVER_PORT } from "./environment/environment";
import errorMiddleware from "./middleware/ErrorMiddleware";
import ItemRouter from "./routes/Item.routes";


export default class Server {
  public app: express.Application;
  private port: number;

  constructor() {
    this.app = express();
    this.port = SERVER_PORT;
  }

  public async startServer(callback: any) {
    try {

      // iniciar los middlewares
      this.startMiddlewares();

      // iniciar las rutas API
      this.startRoutes();

      // levantar el servidor en el puerto this.port
      this.app.listen(this.port, callback);
    } catch (error) {
      console.log(error);
    }
  }

  private startMiddlewares() {
    this.app.use(express.json());
    this.app.use(morgan("dev"));
    this.app.use(cors());
  }

  private async startRoutes() {
    this.app.use("/api/items", new ItemRouter().router);
    this.app.use(errorMiddleware);
  }
}
