const Constants = {
    URL_API_MELI: "https://api.mercadolibre.com",
    ITEMS: "/items",
    CATEGORIES: "/categories",
    LIST_ITEMS: "/sites/MLA/search"
  };
  
  export default Constants;
  