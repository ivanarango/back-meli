import app from "./app";
import { SERVER_PORT } from "./environment/environment";
const server = new app();

server.startServer(() => console.log(`server on port ${SERVER_PORT}`));
