import { IResponseGetItems } from "../interfaces/IResponseGetItems";
import { IResponseItemDetail } from "../interfaces/IResponseItemDetail";

export interface GetItemDetailUseCase {
  getDetail(itemId: string): Promise<IResponseItemDetail>;
}