import axios from "axios";
import Constants from "../../constants/Constants";
import HttpException from "../../Error-handler/HttpException";
import { IResponseItemDetail } from "../../interfaces/IResponseItemDetail";
import { ItemMapper } from "../../mappers/ItemMapper";
import { GetItemDetailUseCase } from "../getItemDetail-interface";

export default class GetItemDetailImpl implements GetItemDetailUseCase {
  private itemsMapper: ItemMapper;

  constructor() {
    this.itemsMapper = new ItemMapper();
  }

  async getDetail(itemId: string): Promise<IResponseItemDetail> {
    try {
      const item = await axios.get(
        `${Constants.URL_API_MELI + Constants.ITEMS}/${itemId}`
      );
      const description = await axios.get(
        `${Constants.URL_API_MELI +Constants.ITEMS}/${itemId}/description`
      );
      const categories = await axios.get(
        `${Constants.URL_API_MELI + Constants.CATEGORIES}/${item.data.category_id}`
      );
      return this.itemsMapper.itemDetailMapper(
        item.data,
        description.data.plain_text,
        categories.data.path_from_root
      );
    } catch (error: any) {
      console.error(`Error getting detail item , cause: [${error.message}]`);
      if (error.isAxiosError) {
        throw new HttpException(error.response.status, error.message);
      }
      throw new HttpException(500, "Sorry internal Error getting detail item");
    }
  }
}
