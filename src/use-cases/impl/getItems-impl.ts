import axios from "axios";
import Constants from "../../constants/Constants";
import HttpException from "../../Error-handler/HttpException";
import { IResponseGetItems } from "../../interfaces/IResponseGetItems";
import { ItemMapper } from "../../mappers/ItemMapper";
import { GetItemsUseCase } from "../getItems-interface";

export default class GetItemsImpl implements GetItemsUseCase {
  private itemsMapper: ItemMapper;

  constructor() {
    this.itemsMapper = new ItemMapper();
  }
  async getListItems(query: string): Promise<IResponseGetItems> {
    try {
      const responseApi = await axios.get(
        `${Constants.URL_API_MELI + Constants.LIST_ITEMS}`,
        {
          params: {
            q: query,
            limit: 4,
          },
        }
      );
      const items = this.itemsMapper.mapList(responseApi.data.results);
      const categories = await axios.get(
        `${Constants.URL_API_MELI + Constants.CATEGORIES}/${this.getCategory(
          responseApi.data.results
        )}`
      );
      return {
        items,
        categories: this.itemsMapper.mapCategories(
          categories.data.path_from_root
        ),
      };
    } catch (error: any) {
      console.error(`Error getting list items , cause: [${error.message}]`);
      if (error.isAxiosError) {
        throw new HttpException(error.response.status, error.message);
      }
      throw new HttpException(500, "Sorry internal Error getting list items");
    }
  }

  private getCategory(items: any) {
    let result = items.reduce(function (prev: any, cur: any) {
      prev[cur.category_id] = (prev[cur.category_id] || 0) + 1;
      return prev;
    }, {});
    return Object.keys(result).reduce((a, b) =>
      result[a] > result[b] ? a : b
    );
  }
}
