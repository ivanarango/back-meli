import { IResponseGetItems } from "../interfaces/IResponseGetItems";

export interface GetItemsUseCase {
  getListItems(query: string): Promise<IResponseGetItems>;
}
