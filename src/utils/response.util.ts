import { AUTHOR_LASTNAME, AUTHOR_NAME } from "../environment/environment";

export default class UtilResponse {
  static buildResponse(data?: any) {
    return {
      author: {
        name: AUTHOR_NAME,
        lastname: AUTHOR_LASTNAME,
      },
      ...data,
    };
  }
}
