export const SERVER_PORT: number = Number(process.env.PORT) || 3100;
export const AUTHOR_NAME: string = "Ivan David";
export const AUTHOR_LASTNAME: string = "Arango Lizarazo";