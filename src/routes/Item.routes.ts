import { Router } from "express";
import ItemController from "../controllers/item.controller";

export default class ItemRouter {
  public router: Router;
  public controller: ItemController;

  constructor() {
    this.router = Router();
    this.controller = new ItemController();
    this.setupRoutes();
  }
  private setupRoutes(): void {
    this.router.get("/", this.controller.getItems.bind(this.controller));
    this.router.get("/:id", this.controller.getItem.bind(this.controller));
  }
}
