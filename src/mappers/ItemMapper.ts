import { response } from "express";
import { IResponseItem } from "../interfaces/IResponseItem";
import { IResponseItemDetail } from "../interfaces/IResponseItemDetail";

export class ItemMapper {
  constructor() {}

  public map(input: any): IResponseItem {
    let state;
    if (input.address) {
      state = input.address.state_name;
    } else {
      state = input.seller_address.state.name;
    }
    return {
      id: input.id,
      title: input.title,
      picture: input.thumbnail,
      price: {
        amount: input.price,
        currency: input.currency_id,
        decimals: 2,
      },
      condition: input.condition,
      free_shipping: input.shipping.free_shipping,
      state,
    };
  }

  public mapList(input: any[]): IResponseItem[] {
    return input.map((item) => this.map(item));
  }

  public itemDetailMapper(
    input: any,
    description: string,
    categories: string[]
  ): IResponseItemDetail {
    const item = this.map(input);
    return {
      item: {
        ...item,
        sold_quantity: input.sold_quantity,
        description,
      },
      categories: this.mapCategories(categories),
    };
  }

  public mapCategories(categories: any) {
    return categories.map((category: any) => category.name);
  }
}
