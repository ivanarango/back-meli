import { NextFunction, Request, Response } from "express";
import { GetItemDetailUseCase } from "../use-cases/getItemDetail-interface";
import { GetItemsUseCase } from "../use-cases/getItems-interface";
import GetItemDetailImpl from "../use-cases/impl/getItemDetail-impl";
import GetItemsImpl from "../use-cases/impl/getItems-impl";
import UtilResponse from "../utils/response.util";

export default class ItemController {
  private getItemsUseCase: GetItemsUseCase;
  private getItemDetailUseCase: GetItemDetailUseCase;

  constructor() {
    this.getItemsUseCase = new GetItemsImpl();
    this.getItemDetailUseCase = new GetItemDetailImpl();
  }

  public async getItems(
    request: Request,
    response: Response,
    next: NextFunction
  ) {
    try {
      const query: string = String(request.query.q);

      return response.json(
        UtilResponse.buildResponse(
          await this.getItemsUseCase.getListItems(query)
        )
      );
    } catch (error) {
    next(error);
    }
  }

  public async getItem(
    request: Request,
    response: Response,
    next: NextFunction
  ) {
    try {
      const { id } = request.params;

      return response.json(
        UtilResponse.buildResponse(
          await this.getItemDetailUseCase.getDetail(id)
        )
      );
    } catch (error) {
      next(error);
    }
  }
}
