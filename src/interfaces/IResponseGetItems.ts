import { IResponseItem } from "./IResponseItem";

export interface IResponseGetItems {
    categories: string [];
    items: IResponseItem [];
}