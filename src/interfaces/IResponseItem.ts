import { IResponsePrice } from "./IResponsePrice";

export interface IResponseItem {
  id: string;
  title: string;
  price: IResponsePrice;
  picture: string;
  condition: string;
  free_shipping: boolean;
  state?: string;
}
