export interface IResponsePrice {
  currency: string;
  amount: number;
  decimals: number;
}
