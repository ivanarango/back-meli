import { IItemDetail } from "./IItemDetail";

export interface IResponseItemDetail {
  item: IItemDetail;
  categories: String [];
}
