import { IResponseItem } from "./IResponseItem";

export interface IItemDetail extends IResponseItem {
  sold_quantity: number;
  description: string;
}
