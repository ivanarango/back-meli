# Api meli

API para prueba tecnica de mercado libre consulta de items y detalle de item

## Available Scripts

dentro del directorio del proyecto:

### `npm run install`

instalar dependencias

### `npm run build`

contruir el proyecto (transpilar de typescript a javascript)

### `npm run start`

iniciar servidor en el puerto 3100.

### `npm run dev`

iniciar servidor en modo desarrollo.

